#include <cstdlib>

int sensorPin = 2;    // select the input pin for the potentiometer
int ledPin = 13;      // select the pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor
FILE *_magnetLogFile = fopen("/home/alex/log.txt", "a");


void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
  sensorValue = digitalRead(sensorPin);
  if (!sensorValue) {
    digitalWrite(ledPin, HIGH);
    system("python /home/alex/daemons/twitter.py \'Magnet found!\'");
  }
  else {
    digitalWrite(ledPin, LOW);
  }
  fprintf(_magnetLogFile, "%d\n", sensorValue);  
  fflush(_magnetLogFile);
  delay(1000);
}
