#include <cstdio>
#include <cstdlib>
#include "dht/DHT11.h"

int pin=4;
DHT11 dht11(pin);


FILE *_tempLogFile = fopen("/home/alex/log_temp.txt", "a");

void setup()
{
}

void loop()
{
  int err;
  float temp, humi;
  if((err=dht11.read(humi, temp))==0)
  {
      fprintf(_tempLogFile, "%f %f\n", humi, temp);   
      fflush(_tempLogFile);
  }
  else
  {
    fprintf(_tempLogFile, "Error %d\n", err);
    fflush(_tempLogFile);
  }
  fprintf(_tempLogFile, "LOG");
  fflush(_tempLogFile);
  delay(DHT11_RETRY_DELAY); //delay for reread
}



