import serial
import mraa
import time
import datetime

import sensors

uart = mraa.Uart(0)
ser = serial.Serial(uart.getDevicePath(), 115200)

# buffer values


# Fetch ALL data from arduino
# 0 - temp2
# 1 - hum
# 2 - IR
# 3 - gps
# 4 - acc
# 5 - RFID
# 6 - sound
def check_treasure(temp, hum, magnet, light, ir, sound):

    return ''


def send_package(package):
    return


if __name__ == '__main__':
    old_data = ''
    while True:
        data = ser.readline()[:-2]
        if data and data != old_data:
            package = list()
            old_data = data
            tokens = data.split('|')
            if len(tokens) != 7:
                continue

            timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

            # to be processed
            temp2 = tokens[0]  # env
            hum = tokens[1]
            magnet = sensors.readMagnet()
            light = sensors.readLight()
            ir = tokens[2]
            sound = tokens[6]

            treasure = check_treasure(temp2, hum, magnet, light, ir, sound)

            # no processing
            temp1 = sensors.readTemp()  # body
            gps = tokens[3]
            steps = tokens[4]
            rfid = tokens[5]

            # timestamp
            # temp1 body
            # tem2 air
            # humi
            # steps
            # gps
            # treasure found
            # treasure type
            # rfid
            package.append(timestamp)
            package.append(str(temp1))
            package.append(str(temp2))
            package.append(str(hum))
            package.append(str(steps))
            package.append(gps)
            package.append(str(len(treasure) > 0))
            package.append(treasure)
            package.append(rfid)

            result = "|".join(package)

            send_package(result)
            print(result)
            # print(str(len(tokens)) + ' ~ ' + "".join(tokens))
            # print(magnet)
            # time.sleep(0.5)
        else:
            print('old: ' + old_data)
            print('data: ' + data)

