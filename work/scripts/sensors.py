import mraa
import time
import smbus

magnetPinNumber = 13
magnetSensor = mraa.Gpio(magnetPinNumber)

def readMagnet():
    return not magnetSensor.read()

def readLight():
    return 228

def readTemp():
    return 32

if __name__ == '__main__':
    while True:
        magnetValue = readMagnet(magnetSensor)
        print(magnetValue)
        time.sleep(0.05)

