import urllib2
import sys
from urllib import quote

def tweet(message):
	url = "http://91.195.12.157:8080/analyzer-spring/twitter/post/"
        url += quote(message)
	response = urllib2.urlopen(url)
	return response.read()

if __name__ == '__main__':
	print(tweet("Unit test #1"))
