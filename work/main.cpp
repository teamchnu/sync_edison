#include<stdio.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<termios.h>
#include<sys/select.h>
#define BAUDRATE 115200
#define DEVICE "/dev/ttyMFD1"
int serial_read(int fd, char *str, unsigned int len, unsigned int timeout)
{
    fd_set rfds;
    struct timeval tv;
    int ret;               
    int sret;             
    int readlen = 0;    
    char *ptr;
 
    ptr = str;           
 
    FD_ZERO(&rfds);        
    FD_SET(fd, &rfds);    
 
 
    tv.tv_sec = timeout / 1000;
    tv.tv_usec = (timeout % 1000) * 1000;
 
 
    while(readlen < len)
    {
        sret = select(fd + 1, &rfds, NULL, NULL, &tv);
 
        if(sret == -1)
        {
            perror("select:");
            break;
        }
        else if(sret > 0)
        {
            ret = read(fd, ptr, 1);
            printf("sec: %d, usec: %d\n", tv.tv_sec, tv.tv_usec);
            if(ret <= -1)
            {
                perror("read err:");
                break;
            }
 
            else if(ret == 0)
            {
                break;
            }
 
            readlen += ret;
            ptr += ret;
        }
        else
        {
            printf("timeout! \n");
            break;
        }
 
    }
 
    return readlen;
}
 
/* OpenPort:Open Serial port1
* Return the file descriptor on success or -1 on error
*
*/
int OpenPort(void)
{
    int fd;
 
    fd = open(DEVICE, O_RDWR | O_NOCTTY | O_NDELAY);
    if(fd == -1)
    {
        printf("OpenPort:Unable to open /dev/ttyMFD1");
    }
    else
    {
        fcntl(fd, F_SETFL, 0);
        return (fd);
    }
}
int main(int argc, char **argv)
{
    int fd;
    struct termios oldtio, newtio;
    char buf[100] = {};
 
     fd = OpenPort();
     tcgetattr(fd, &oldtio);
     bzero(&newtio, sizeof(newtio));
    
     newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
     newtio.c_iflag = IGNPAR | ICRNL;
     newtio.c_oflag = 0;
     newtio.c_lflag = ICANON;
     newtio.c_cc[VINTR] = 0; /* Ctrl-c */
     newtio.c_cc[VQUIT] = 0; /* Ctrl-\ */
     newtio.c_cc[VERASE] = 0; /* del */
     newtio.c_cc[VKILL] = 0; /* @ */
     newtio.c_cc[VEOF] = 4; /* Ctrl-d */
     newtio.c_cc[VTIME] = 0; /* inter-character timer unused */
                           
     newtio.c_cc[VMIN] = 1; /* blocking read until 1 character arrives */
                               
     newtio.c_cc[VSWTC] = 0; /* '\0' */
     newtio.c_cc[VSTART] = 0; /* Ctrl-q */
     newtio.c_cc[VSTOP] = 0; /* Ctrl-s */
     newtio.c_cc[VSUSP] = 0; /* Ctrl-z */
     newtio.c_cc[VEOL] = 0; /* '\0' */
     newtio.c_cc[VREPRINT] = 0; /* Ctrl-r */
     newtio.c_cc[VDISCARD] = 0; /* Ctrl-u */
     newtio.c_cc[VWERASE] = 0; /* Ctrl-w */
     newtio.c_cc[VLNEXT] = 0; /* Ctrl-v */
     newtio.c_cc[VEOL2] = 0; /* '\0' */
 
     tcflush(fd, TCIFLUSH);
     tcsetattr(fd, TCSANOW, &newtio);
 
    int n = serial_read(fd, buf, 1, 3000);
 
    //int n = read(fd, buf, 2);
    if(n < 0)
    {
        fputs("read error", stderr);
    }
    printf("%s\n",buf);
}
